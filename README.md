Cordra Tool
===========

This tool assists in developing and testing types and type methods in [Cordra](https://www.cordra.org).
This tool requires the main Cordra source code, which is available [here](https://gitlab.com/cnri/cordra/cordra).

Directories
-----------

This tool assumes certain files and directories will be in certain places within your project.
After you `cd` into your project directory, run:

  `npm install @cnri/cordra-tool`

Create the following directory structure.

  * `cordra` - contains files to support the test runner, described more below
  * `src/main/cordra` - Cordra objects being developed go here:
    * `design` - design.json and design.ts (and other TS/JS files) will be put on the design object
    * `types` -  schema.json files and/or TS/JS files for each type and/or object.json files
    * `types/original` -  schema.json files before transformation
    * `types/interfaces` - TypeScript interfaces generated from the schemas
    * `objects` - json files containing one or more objects, in search results shape
    * `payloads` - if an object has a payload, the loader will look here for it
  * `src/test` - Files used by the test runner:
    * `cordra.test.ts` - main test entry point
    * `resources` - misc files used by tests
    * `tests` - test suites loaded by the main runner in `cordra.test.ts`
  * `src/test/cordra` - Files to be installed in Cordra during testing, same structure as `src/main/cordra`

The files under the `types` directory should start with the name of the Cordra Type and end in `.schema.json`,
`.object.json`, `.js`, or `.ts`. For example, for the type `Document`, you might have `Document.schema.json`,
`Document.object.json`, and one of either `Document.js` or `Document.ts`.

If the object.json file exists, it will be parsed into a Cordra Object representing a schema. This can be used to set
object-level properties like `hashObject`. If the schema.json file exists, it will be set as the schema on the object,
overwriting anything in object.json. If a typescript file exists, it will be compiled into Javascript and set as the
javascript property of the object. If a Javascript file exists, it will be directly set as the javascript property of
the object.

If object instances have a payload with a filename, the loader will look for that file in the `payloads` directory and,
if found, attach it to the object. If you would like to add payloads to the design object, create a file called
`payloads.json` in the `design` directory. This file should contain a list of payload objects. For example:

```json
[
  {
    "name": "home",
    "filename": "home.html",
    "mediaType": "text/html"
  },
  {
    "name": "passwordReset",
    "filename": "passwordResetForm.html",
    "mediaType": "text/html"
  }
]
```

Command Line Tool
-----------------

This project contains a command line tool for inserting digital objects into Cordra. To see the options for the tool, run:

  `npx cordra-tool -h`

***WARNING***: This tool will delete things if you ask it to! Don't point it at a production machine!

To transform the schemas and generate the TypeScript interfaces from the schemas:

  `npx cordra-tool --transform --transformer build/main/SchemaTransformer.js`
  `npx cordra-tool --interfaces`

All other commands need to be run after `npm run build`.

To delete everything and set up the design and types:

  `npx cordra-tool -c cordra/client-config.json -q -d -t`

You can use this tool to load the sample objects using this command:

  `npx cordra-tool -c cordra/client-config.json -o src/main/cordra/objects/FILE`

To load using a pool of 20 workers:

  `CORDRA_UPDATER_ASYNC=true npx cordra-tool -c cordra/client-config.json -o src/main/cordra/objects/FILE`

To delete all non-type cordra objects using a pool of 20 workers:

  `CORDRA_UPDATER_ASYNC=true npx cordra-tool -c cordra/client-config.json -x`

To delete all cordra objects including types using `-q` instead of `-x`.

You can use `npm run cordra:setup` to build this project then install the design and types into the Cordra specified in `cordra/client-config.json`.


Development
-----------

To enable the Cordra runner, place `cordra.war` and `cordra-embedded-test-server.jar` into `cordra` folder. Both can be built from the main Cordra project source code. Place any additional jar files to be added to the classpath into `cordra/lib`.

Once that is done, the following command will run a temporary Cordra instance, start a Babel compilation watcher, and start an upload watcher all in one terminal:

  `npm run cordra:dev`

Now, edit design, types, objects, and payloads in your IDE. When you save a file, it should automatically be updated in the running Cordra instance.

You can also run Cordra and the various watchers individually if desired. To run an empty Cordra instance that will be deleted on close:

`npm run cordra:run`

To start the Babel compile watcher, to re-compile files as they are changed:

`npm run watch`

To start the Cordra upload watcher, which will watch for Type and Design object changes and load them into Cordra:

`npm run cordra:watch`

Tests
-----

Tests require a running Cordra. By default, one will be spun up before the tests start. To enable this, place `cordra.war` and `cordra-embedded-test-server.jar` in the `cordra` folder. Then, run the tests with this command:

`npm run test:cordra`

Equivalently, you can `npm run cordra:run` to start up a test cordra, and `npm run test` to run the tests against it.
(You could then clear "initialized" in `cordra/client-config.json` and `npm run test` again...)

You can also point the tests at an existing Cordra.

***WARNING!!! THE TEST SETUP WILL DELETE THINGS!***

To point at an existing Cordra, create the file `cordra/client-config.json`
so it looks something like this:

```json
{
  "baseUri": "http://localhost:62394/cordra/",
  "username": "admin",
  "password": "changeit",
  "initialized": false
}
```

Set initialized to true if the Cordra has already been loaded with types and config. If initialized is false, the test runner will *DELETE EVERYTHING* in Cordra and then load the types, before proceeding with the tests.

If you start Cordra using `npm run cordra:run`, the config file will be created automatically.

Once the file is created and Cordra is started, run the tests once with:

`npm run test`

or in watch mode with:

`npm run test:watch`

License
-------

See the [LICENSE.txt](https://hdl.handle.net/20.1000/133) file for details.
