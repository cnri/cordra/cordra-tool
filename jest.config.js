/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/en/configuration.html
 */

module.exports = {
    // A path to a module which exports an async function that is triggered once before all test suites
    // globalSetup: '<rootdir>/test/env/setup.js', TODO parallel tests

    // A path to a module which exports an async function that is triggered once after all test suites
    // globalTeardown: '<rootdir>/test/env/teardown.js', TODO parallel tests

    // Allow time for loading Cordra objects
    testTimeout: 60_000,
    moduleNameMapper: {
        '/cordra/schemas/(.*)': [
            '<rootDir>/build/main/cordra/types/$1.js',
            '<rootDir>/build/test/cordra/types/$1.js'
        ]
    },
    testEnvironment: 'node',
    testPathIgnorePatterns: [
        '<rootDir>/node_modules/',
        // Need to watch build folder, since the tests load files from there.
        '<rootDir>/build/'
    ],
    // Ignore changes in src so that jest only triggers after files have been compiled
    watchPathIgnorePatterns: [
        '<rootDir>/src/main/cordra',
        '<rootDir>/src/test/cordra/'
    ]
};
