/* eslint-disable @typescript-eslint/no-explicit-any */

// TODO consider any vs unknown
// TODO investigate null vs undefined

declare module "cordra" {
    // a limitation of TS ambient modules is they can not have top-level import statements
    export type CordraObject = import('@cnri/cordra-client').CordraObject;
    export type AccessControlList = import('@cnri/cordra-client').AccessControlList;
    export type Metadata = import('@cnri/cordra-client').Metadata;
    export type Payload = import('@cnri/cordra-client').Payload;

    export type QueryParams = import('@cnri/cordra-client').QueryParams;
    export type SortField = import('@cnri/cordra-client').SortField;
    export type FacetSpecification = import('@cnri/cordra-client').FacetSpecification;

    export type SearchResults<T> = import('@cnri/cordra-client').SearchResults<T>;
    export type FacetResult = import('@cnri/cordra-client').FacetResult;
    export type FacetBucket = import('@cnri/cordra-client').FacetBucket;

    export class CordraError extends Error {
        constructor(message?: string, responseCode?: number);
        constructor(response?: unknown, responseCode?: number);

        responseCode?: number;
        response?: unknown;
    }

    export interface Context {
        isSearch?: boolean;
        isNew?: boolean;
        objectId: string;
        userId?: string;
        groups?: string[];
        effectiveAcl?: { readers: string[]; writers: string[] };
        aclCreate?: string[];
        newPayloads?: Payload[];
        payloadsToDelete?: string[];
        requestContext?: any;
        payload?: string;
        start?: number;
        end?: number;
        params?: any;
        attributes?: any;
        isDryRun?: boolean;
        method?: string;
        originalObject?: CordraObject;
        beforeSchemaValidationResult?: CordraObject;
        directIo: {
            getInputAsUint8Array(): Uint8Array;
            getInputAsString(): string;
            getInputAsJson(): any;
            getInputAsJavaInputStream(): unknown;
            getInputAsJavaReader(): unknown;
            writeOutputUint8Array(bytes: Uint8Array): void;
            writeOutputString(string: string): void;
            writeOutputJson(json: any): void;
            getOutputAsJavaOutputStream(): unknown;
            getOutputAsJavaWriter(): unknown;
            getInputFilename(): string | null | undefined;
            getInputMediaType(): string | null | undefined;
            setOutputFilename(filename: string): void;
            setOutputMediaType(mediaType: string): void;
        };
    }

    export function get(id: string, full: false): any | null | undefined;
    export function get(id: string, full?: true): CordraObject | null | undefined;

    export function search(query: string, params?: QueryParams): SearchResults<CordraObject>;

    export function getPayloadAsUint8Array(id: string, payloadName: string): Uint8Array | null | undefined;
    export function getPayloadAsString(id: string, payloadName: string): string | null | undefined;
    export function getPayloadAsJson(id: string, payloadName: string): any | null | undefined;
    export function getPayloadAsJavaInputStream(id: string, payloadName: string): unknown;
    export function getPayloadAsJavaReader(id: string, payloadName: string): unknown;
    export function getPartialPayloadAsUint8Array(id: string, payloadName: string, start?: number, end?: number): Uint8Array | null | undefined;
    export function getPartialPayloadAsString(id: string, payloadName: string, start?: number, end?: number): string | null | undefined;
    export function getPartialPayloadAsJavaInputStream(id: string, payloadName: string, start?: number, end?: number): unknown;
    export function getPartialPayloadAsJavaReader(id: string, payloadName: string, start?: number, end?: number): unknown;
}

declare module "cordraUtil" {
    // a limitation of TS ambient modules is they can not have top-level import statements
    export type CordraObject = import('@cnri/cordra-client').CordraObject;
    export type JsonWebKey = import('@cnri/cordra-client').JsonWebKey;

    export interface VerificationReport {
        content?: boolean;
        userMetadata?: boolean;
        full?: boolean;
        payloads?: { [payloadName: string]: boolean };
    }

    export type JwsJson = FlattenedJwsJson | GeneralJwsJson;
    export interface FlattenedJwsJson {
        payload: string;
        protected?: string;
        header?: string;
        signature: string;
    }
    export interface GeneralJwsJson {
        payload: string;
        signatures: Array<{
            protected?: string;
            header?: string;
            signature: string;
        }>;
    }

    export function hashJson(jsElement: any, algorithm?: string): string;
    export function escapeForQuery(phrase: string): string;
    export function verifySecret(obj: string | CordraObject, jsonPointer: string, secret: string): boolean;
    export function verifyHashes(obj: CordraObject): VerificationReport;
    export function signWithKey(payload: any, key: JsonWebKey, useJsonSerialization: true): JwsJson;
    export function signWithKey(payload: any, key: JsonWebKey, useJsonSerialization?: false): string;
    export function signWithCordraKey(payload: any, useJsonSerialization: true): JwsJson;
    export function signWithCordraKey(payload: any, useJsonSerialization?: false): string;
    export function getCordraPublicKey(): JsonWebKey;
    export function verifyWithCordraKey(jwt: string): boolean;
    export function extractJwtPayload(jwt: string): any;
    export function getDoipProcessorConfig(): any;
}
