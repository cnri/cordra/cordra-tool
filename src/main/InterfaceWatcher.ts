import chokidar, { FSWatcher } from 'chokidar';
import path from 'path';
import { createAllTypeScriptInterfaces, createTypeScriptInterface } from './InterfaceGenerator';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

export class InterfaceWatcher {
    private readonly rootDir: string;
    private readonly outDir: string;
    private readonly userIgnored: (RegExp | string)[];
    private ready = false;
    private watcher: FSWatcher | undefined;

    constructor(root = __dirname, out: string, userIgnored: (RegExp | string)[]) {
        this.rootDir = path.resolve(root);
        this.outDir = out ? path.resolve(out) : path.resolve(this.rootDir, 'types', 'interfaces');
        this.userIgnored = userIgnored;
    }

    run(): void {
        const watchPaths = [
            path.resolve(this.rootDir, 'types')
        ];
        const ignored: (RegExp | string)[] = [
            /(^|[/\\])\../, // dotfiles
            '**/*.map',
            '**/*.d.js'
        ];
        ignored.push(...this.userIgnored);
        this.watcher = chokidar.watch(watchPaths, {
            ignoreInitial: true,
            ignored,
            persistent: true
        });

        this.watcher
        .on('ready', () => this.afterReady().catch(console.log))
        .on('error', error => console.log((`Watcher error: ${JSON.stringify(error)}`)))
        .on('add', file => this.createOrUpdate(file).catch(console.log))
        .on('change', file => this.createOrUpdate(file).catch(console.log))
        .on('unlink', file => this.removeObject(file).catch(console.log));
    }

    async createOrUpdate(file: string): Promise<void> {
        if (!this.ready) return;
        if (path.dirname(path.dirname(file)) !== this.rootDir) return;
        const schemaMatches = /([^/]+).schema.json$/.exec(file);
        if (schemaMatches && schemaMatches.length > 1) {
            const name = schemaMatches[1];
            console.log(`Updating TypeScript interface for ${name}`);
            await createTypeScriptInterface(file, this.outDir);
        }
    }

    async removeObject(file: string): Promise<void> {
        if (!this.ready) return;
        console.log(`File ${file} has been removed. Interface not deleted.`);
    }

    async afterReady(): Promise<void> {
        if (!this.watcher) {
            throw new Error('Watcher not initialized before ready call');
        }
        const schemasDir = path.resolve(this.rootDir, 'types');
        await createAllTypeScriptInterfaces(schemasDir, this.outDir);
        this.ready = true;
        console.log('Interface watcher ready for changes');
    }
}
