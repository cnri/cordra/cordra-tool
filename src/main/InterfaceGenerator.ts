import path from "path";
import fs from "fs";
import { compile } from "json-schema-to-typescript";

// function untitle(json: unknown, deleteTitleAtTopLevel: boolean = true): void {
//     if (json === null || json === undefined) return;
//     if (typeof json === 'string') return;
//     if (typeof json === 'number') return;
//     if (typeof json === 'boolean') return;
//     if (Array.isArray(json)) {
//         json.forEach(x => untitle(x));
//     }
//     const obj = json as Record<string, unknown>;
//     if (deleteTitleAtTopLevel) delete obj.title;
//     for (const key of Object.keys(obj)) {
//         if (key === 'enum') continue;
//         if (key === 'properties' || key === 'patternProperties') untitle(obj[key], false);
//         untitle(obj[key]);
//     }
// }

export async function createTypeScriptInterface(schemaJsonPath: string, outDir: string): Promise<void> {
    const schemaString = fs.readFileSync(schemaJsonPath, { encoding: 'utf-8' });
    const schemaJson = JSON.parse(schemaString);
    // Currently the titles need to be put in only where they help
    // untitle(schemaJson); // Note titles in other files accessed by $ref are still used
    const name = path.basename(schemaJsonPath).replace('.schema.json', '');
    const ts = await compile(schemaJson, name, {
        bannerComment: '/* eslint-disable */',
        cwd: path.dirname(schemaJsonPath),
        declareExternallyReferenced: true,
        ignoreMinAndMaxItems: true,
        style: { tabWidth: 4 },
        unreachableDefinitions: true
    });
    const outPath = path.resolve(outDir, `${name}.d.ts`);
    fs.mkdirSync(outDir, { recursive: true });
    fs.writeFileSync(outPath, ts);
}

export async function createAllTypeScriptInterfaces(schemasDir: string, outDir: string): Promise<void> {
    const files = await fs.promises.opendir(schemasDir);
    for await (const file of files) {
        if (!file.name.endsWith('.schema.json')) continue;
        await createTypeScriptInterface(path.resolve(schemasDir, file.name), outDir).catch(console.log);
    }
}
