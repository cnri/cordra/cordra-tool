#!/usr/bin/env node
import fs from 'fs';
import path from 'path';
import { program } from 'commander';
import { CordraClient } from "@cnri/cordra-client";
import { CordraUpdater } from "./CordraUpdater";
import { CordraWatcher } from './CordraWatcher';
import { createAllTypeScriptInterfaces } from './InterfaceGenerator';
import { findPackageRoot } from './util';
import { InterfaceWatcher } from './InterfaceWatcher';

program
.option('-c, --config <file>', 'Config file containing Cordra credentials')
.option('-b, --base-uri <base-uri>', 'Cordra Base Uri', 'https://localhost:8443/')
.option('-u, --username <username>', 'Cordra username', 'admin')
.option('-p, --password <password>', 'Cordra password')
.option('-r, --root-dir <root-dir>', 'Root directory for files to be loaded')
.option('--interfaces-out-dir <output-dir>', 'Output directory for interface files')
.option('-x, --delete-all', 'Delete all non-Type objects in Cordra')
.option('-q, --delete-all-including-types', 'Delete all objects in Cordra (implies --delete-all)')
.option('-t, --types', 'Update types')
.option('-d, --design', 'Update Design object')
.option('-o, --objects <objects-file...>', 'Update objects in from file')
.option('-k, --insecure', 'Do not verify Cordra SSL certificate. Enable if using a self-signed cert.')
.option('-s, --reset', 'Resets Cordra to default types and design. Happens before any other changes. Implies delete all.')
.option('-w, --watch', 'Watch for file changes and update Cordra')
.option('-i, --interfaces', 'Write TypeScript interfaces for schemas in src/main/cordra to src/main/cordra/interfaces')
.option('--watch-objects', 'Watch for object file changes and update Cordra objects')
.option('--watch-interfaces', 'Watch for file changes and update TypeScript interfaces')
.option('--transform', 'Transform original schemas from src/main/cordra/original to src/main/cordra')
.option('--transformer <js-file>', 'Transform using this file\'s export transform: (name: string, json: any) => boolean')
.option('--force', 'Enable running potentially destructive operations on a non-local Cordra')
.option('--ignored <ignored-file>', 'Ignore files and folders with paths in a file at this path');

main().catch(console.error);

async function main(): Promise<void> {
    const args = acquireArgs();
    const rootDir = args.rootDir ?? path.join(findPackageRoot(), 'build', 'main', 'cordra');
    const interfacesOutDir = args.interfacesOutDir ?? path.join(findPackageRoot(), 'build', 'main', 'cordra', 'types', 'interfaces');
    let client;
    let updater: CordraUpdater | undefined;
    const ensureUpdater = () => {
        if (updater) return updater;
        if (args.config) {
            const config = JSON.parse(fs.readFileSync(args.config, 'utf8'));
            client = new CordraClient(config.baseUri, { username: config.username, password: config.password });
        } else {
            if (!args.password) {
                throw new Error('No password given');
            }
            client = new CordraClient(args.baseUri, { username: args.username, password: args.password });
        }
        const isLocalhost = client.baseUri.includes('localhost') || client.baseUri.includes('127.0.0.1');
        if (!isLocalhost && (args.reset || args.deleteAll || args.deleteAllIncludingTypes)) {
            if (!args.force) {
                throw new Error('Trying to run a destructive operation on a non-local Cordra! If you are sure, rerun with the --force flag');
            }
            console.log('WARNING: Force running a potentially destruction operation on a non-local Cordra. Hope you meant that');
        }
        updater = new CordraUpdater(client, rootDir);
        return updater;
    };
    if (args.watch || args.watchInterfaces) {
        const userIgnored = getUserIgnoreList(args.ignored);
        if (args.watch) {
            const watcher = new CordraWatcher(
                ensureUpdater(),
                rootDir,
                false,
                args.watchObjects,
                userIgnored);
            watcher.run();
        }
        if (args.watchInterfaces) {
            const interfaceWatcher = new InterfaceWatcher(rootDir, interfacesOutDir, userIgnored);
            interfaceWatcher.run();
        }
        return;
    }
    if (args.reset) {
        await ensureUpdater().deleteObjectsIfNeeded(true, true);
        await ensureUpdater().loadDefaultTypesAndDesign();
    } else if (args.deleteAll || args.deleteAllIncludingTypes) {
        await ensureUpdater().deleteObjectsIfNeeded(args.deleteAll, args.deleteAllIncludingTypes);
    }
    if (args.design) await ensureUpdater().updateDesign();
    if (args.types) await ensureUpdater().createOrUpdateTypes();
    if (args.objects) await ensureUpdater().createOrUpdateObjects(args.objects);
    if (args.interfaces) await createAllTypeScriptInterfaces(path.join(rootDir, 'types'), path.join(rootDir, 'types', 'interfaces'));
    if (args.transform) {
        if (!args.transformer) throw new Error("transformer required");
        // eslint-disable-next-line @typescript-eslint/no-var-requires, global-require, import/no-dynamic-require
        const transformer = require(path.resolve(process.cwd(), args.transformer)).transform;
        await transformAllOriginalSchemas(path.join(rootDir, 'types', 'original'), path.join(rootDir, 'types'), transformer);
    }
    // TODO what about payloads on types?
}

function getUserIgnoreList(ignoreFilePath: string): string[] {
    const ignored = new Set<string>();
    if (ignoreFilePath) {
        const ignoredFile: string = fs.readFileSync(ignoreFilePath, 'utf8');
        ignoredFile.split('\n')
        .map(s => s.trim())
        .filter(s => s.length > 0)
        .forEach(s => {
            ignored.add(path.resolve(s));
        });
    }
    return Array.from(ignored);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function acquireArgs(): any {
    program.parse(process.argv);
    const args = { ...program.opts() };
    if (args.configFile) {
        Object.assign(args, JSON.parse(fs.readFileSync(args.configFile, 'utf8')));
    }
    if (args.insecure) {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    }
    return args;
}

async function transformAllOriginalSchemas(schemasDir: string, outDir: string, transform: (name: string, json: unknown) => boolean): Promise<void> {
    const files = await fs.promises.opendir(schemasDir);
    for await (const file of files) {
        if (!file.name.endsWith('.schema.json')) continue;
        const pathName = path.resolve(schemasDir, file.name);
        const name = file.name.replace('.schema.json', '');
        const jsonString = fs.readFileSync(pathName, 'utf-8');
        const json = JSON.parse(jsonString);
        if (!transform(name, json)) continue;
        const output = JSON.stringify(json, null, 4);
        fs.writeFileSync(path.resolve(outDir, file.name), output, 'utf-8');
    }
}
