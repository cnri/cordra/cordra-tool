import chokidar, { FSWatcher } from 'chokidar';
import path from 'path';
import fs from 'fs';
import { CordraUpdater } from './CordraUpdater';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

export class CordraWatcher {
    private updater: CordraUpdater;
    private readonly rootDir: string;
    private readonly clientInitialized;
    private readonly watchObjects;
    private readonly userIgnored: (RegExp | string)[];
    private ready = false;
    private designNeedsUpdate = false;
    private schemasNeedingUpdate = new Set<string>();
    private objectsNeedingUpdate = new Set<string>();
    private payloadsNeedingUpdate = new Set<string>();
    private updating = false;
    private watcher: FSWatcher | undefined;

    constructor(
            updater: CordraUpdater,
            root = __dirname,
            clientInitialized = false,
            watchObjects = false,
            userIgnored: (RegExp | string)[] = []
    ) {
        this.updater = updater;
        this.rootDir = path.resolve(root);
        this.clientInitialized = clientInitialized;
        this.watchObjects = watchObjects;
        this.userIgnored = userIgnored;
    }

    run(): void {
        const watchPaths = [
            path.resolve(this.rootDir, 'design'),
            path.resolve(this.rootDir, 'types'),
            path.resolve(this.rootDir, 'payloads')
        ];
        if (this.watchObjects) {
            watchPaths.push(path.resolve(this.rootDir, 'objects'));
        }
        const ignored: (RegExp | string)[] = [
            /(^|[/\\])\../, // dotfiles
            '**/*.map',
            '**/*.d.js',
            '**/*.d.ts'
        ];
        ignored.push(...this.userIgnored);
        this.watcher = chokidar.watch(watchPaths, {
            ignoreInitial: true,
            ignored,
            persistent: true
        });

        this.watcher
        .on('ready', () => this.afterReady().catch(console.log))
        .on('error', error => console.log((`Watcher error: ${JSON.stringify(error)}`)))
        .on('add', file => this.createOrUpdate(file).catch(console.log))
        .on('change', file => this.createOrUpdate(file).catch(console.log))
        .on('unlink', file => this.removeObject(file).catch(console.log));
    }

    async createOrUpdate(file: string): Promise<void> {
        if (!this.ready) return;
        if (!file.startsWith(this.rootDir)) return;
        if (!fs.existsSync(file)) return;
        if (fs.lstatSync(file).isDirectory()) return;
        if (this.fileIsInDirectory(file, 'design')) {
            this.designNeedsUpdate = true;
            await this.ensureUpdating();
        } else if (this.fileIsInDirectory(file, 'types')) {
            const schemaMatches = /([^/]+).schema.json$/.exec(file);
            if (schemaMatches && schemaMatches.length > 1) {
                const name = schemaMatches[1];
                this.schemasNeedingUpdate.add(name);
                await this.ensureUpdating();
            }
            const objectJsonMatches = /([^/]+).object.json$/.exec(file);
            if (objectJsonMatches && objectJsonMatches.length > 1) {
                const name = objectJsonMatches[1];
                this.schemasNeedingUpdate.add(name);
                await this.ensureUpdating();
            }
            const jsMatches = /([^/]+).js$/.exec(file);
            if (jsMatches && jsMatches.length > 1) {
                const name = jsMatches[1];
                this.schemasNeedingUpdate.add(name);
                await this.ensureUpdating();
            }
        } else if (this.fileIsInDirectory(file, 'payloads')) {
            this.payloadsNeedingUpdate.add(file);
            await this.ensureUpdating();
        } else if (this.fileIsInDirectory(file, 'objects')) {
            this.objectsNeedingUpdate.add(file);
            await this.ensureUpdating();
        }
    }

    fileIsInDirectory(file: string, dir: string): boolean {
        return file.startsWith(path.resolve(this.rootDir, dir));
    }

    async ensureUpdating(): Promise<void> {
        if (this.updating) return;
        this.updating = true;
        await new Promise(resolve => setTimeout(resolve, 1000));
        while (true) {
            if (this.designNeedsUpdate) {
                this.designNeedsUpdate = false;
                await this.updater.updateDesign();
                continue;
            }
            if (this.schemasNeedingUpdate.size > 0) {
                const name = this.schemasNeedingUpdate.values().next().value;
                this.schemasNeedingUpdate.delete(name);
                await this.updater.createOrUpdateTypeByName(name);
                continue;
            }
            if (this.objectsNeedingUpdate.size > 0) {
                const filename = this.objectsNeedingUpdate.values().next().value;
                this.objectsNeedingUpdate.delete(filename);
                await this.updater.createOrUpdateObjects([ filename ]);
                continue;
            }
            if (this.payloadsNeedingUpdate.size > 0) {
                const filename = this.payloadsNeedingUpdate.values().next().value;
                this.payloadsNeedingUpdate.delete(filename);
                await this.updater.createOrUpdateObjectPayload(filename);
                continue;
            }
            this.updating = false;
            return;
        }
    }

    async removeObject(file: string): Promise<void> {
        if (!this.ready) return;
        console.log(`File ${file} has been removed. Object not deleted from Cordra.`);
    }

    async afterReady(): Promise<void> {
        if (!this.watcher) {
            throw new Error('Watcher not initialized before ready call');
        }
        this.ready = true;
        if (!this.clientInitialized) {
            const watched = this.watcher.getWatched();
            for (const key of Object.keys(watched)) {
                const files = watched[key];
                for (const file of files) {
                    this.createOrUpdate(path.resolve(key, file)).catch(console.log);
                }
            }
        }
        console.log('Cordra watcher ready for changes');
    }
}
