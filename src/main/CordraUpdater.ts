import { Blob, CordraClient, CordraObject, Payload } from '@cnri/cordra-client';
import path from "path";
import fs from "fs";
import PromisePool from 'es6-promise-pool';
import { SearchResults } from 'cordra';
import defaultDesign from './defaults/design.json';
import defaultTypes from './defaults/types.json';

interface DesignBits {
    handleMintingConfig?: {
        javascript?: string;
    };
    builtInTypes?: {
        CordraDesign?: {
            javascript?: string;
        };
        Schema?: {
            javascript?: string;
        };
    };
    javascript?: string;
}

export class CordraUpdater {
    private client: CordraClient;
    private readonly rootDir: string;
    private payloadToObjectFileMap: Record<string, string> = {};

    constructor(client: CordraClient, root = __dirname) {
        this.client = client;
        this.rootDir = root;
    }

    async deleteObjectsUsingPool(query: string): Promise<void> {
        const tasks: Array<() => Promise<void>> = [];
        const searchResults: SearchResults<string> = await this.client.searchHandles(query);
        const ids: string[] = searchResults.results;
        for (const id of ids) {
            tasks.push(() => this.performWithRetry(async () => {
                await this.client.delete(id);
            }, `Deleting ${id}`));
        }
        await this.performTasks(tasks);
    }

    private async performWithRetry(task: () => Promise<unknown>, errorMessage: string): Promise<void> {
        let lastError;
        for (let i = 0; i < 10; i++) {
            try {
                await task();
                lastError = undefined;
                break;
            } catch (e) {
                lastError = e;
                await new Promise(resolve => setTimeout(resolve, 200));
            }
        }
        if (lastError) {
            console.error("Error: " + errorMessage);
            console.error(lastError);
        }
    }

    private async performTasks(tasks: (() => Promise<void>)[]): Promise<void> {
        if (process.env.CORDRA_UPDATER_ASYNC) {
            console.log('Using pool');
            const pool = new PromisePool(() => {
                const task = tasks.shift();
                if (!task) return undefined;
                return task();
            }, 20);
            await pool.start();
        } else {
            for (const task of tasks) {
                await task();
            }
        }
    }

    async deleteObjectsIfNeeded(deleteAll: boolean, deleteAllIncludingTypes: boolean): Promise<void> {
        if (!deleteAll && !deleteAllIncludingTypes) {
            console.log('Parameters are both not true: Deleting no objects');
            return;
        }
        console.log('Deleting all non-type objects');
        await this.deleteObjectsUsingPool('*:* -type:Schema');
        if (deleteAllIncludingTypes) {
            console.log('Deleting all types');
            await this.deleteObjectsUsingPool('*:*');
        }
    }


    async createOrUpdateTypes(): Promise<string[]> {
        const schemasPath = path.resolve(this.rootDir, 'types');
        const files = await fs.promises.opendir(schemasPath);
        // const promises = []; // COR-384
        const ids = [];
        for await (const file of files) {
            if (file.name.endsWith('.schema.json')) {
                const name = file.name.replace('.schema.json', '');
                await this.createOrUpdateTypeByName(name).catch(console.log);
            } else if (file.name.endsWith('.object.json')) {
                const name = file.name.replace('.object.json', '');
                if (fs.existsSync(path.resolve(schemasPath, name + '.schema.json'))) continue;
                const id = await this.createOrUpdateTypeByName(name);
                ids.push(id);
            } else if (file.name.endsWith('.js')) {
                const name = file.name.replace('.js', '');
                if (fs.existsSync(path.resolve(schemasPath, name + '.schema.json'))) continue;
                const id = await this.createOrUpdateTypeByName(name);
                ids.push(id);
            }
            // promises.push(createOrUpdateType(client, name, json, js)); // Can't do this async, because of COR-384
        }
        return ids;
        // await Promise.all(promises); // COR-384
    }

    async createOrUpdateTypeByName(name: string): Promise<string> {
        const jsonPath = path.resolve(this.rootDir, `./types/${name}.schema.json`);
        let json = null;
        if (fs.existsSync(jsonPath)) {
            json = fs.readFileSync(jsonPath, 'utf8');
        }
        const jsPath = path.resolve(this.rootDir, `./types/${name}.js`);
        let js = null;
        if (fs.existsSync(jsPath)) {
            js = fs.readFileSync(jsPath, 'utf8');
        }
        const objPath = path.resolve(this.rootDir, `./types/${name}.object.json`);
        let obj = JSON.stringify({
            type: 'Schema',
            content: {
                name,
                schema: {}
            }
        });
        if (fs.existsSync(objPath)) {
            obj = fs.readFileSync(objPath, 'utf8');
        }
        return this.createOrUpdateType(name, json, js, obj);
    }

    async createOrUpdateType(name: string, schema: string | null, js: string | null, obj: string, id?: string): Promise<string> {
        console.log(`Processing Type: ${name}`);
        const object = JSON.parse(obj);
        object.type = 'Schema';
        if (!object.content) {
            object.content = { name };
        }
        if (!object.content.name) object.content.name = name;
        if (schema) object.content.schema = JSON.parse(schema);
        if (!object.content.schema) {
            object.content.schema = {};
        }
        if (id) object.id = id;
        if (js) object.content.javascript = js;

        const existingObjects = await this.client.search('+type:Schema +/name:' + name);
        // FIXME is doing create when it should update
        for (const existingObject of existingObjects.results) {
            if ((existingObject).content.name === name) {
                object.id = existingObject.id;
                break;
            }
        }
        return this.createOrUpdateObject(object);
    }

    async createOrUpdateObjects(objectFiles: string[]): Promise<string[]> {
        const tasks: Array<() => Promise<void>> = [];
        const ids: string[] = [];
        for (const file of objectFiles) {
            const json = fs.readFileSync(file, 'utf8');
            const objects = JSON.parse(json).results as CordraObject[];
            if (!objects) continue;
            let count = -1;
            for (const object of objects) {
                count++;
                if (object.payloads) {
                    for (const payload of object.payloads) {
                        if (!payload.filename) {
                            console.warn('Skipping payload without filename');
                            continue;
                        }
                        this.payloadToObjectFileMap[payload.filename] = file;
                        const filePath = path.resolve(this.rootDir, './payloads', payload.filename);
                        const contentType = payload.mediaType || 'application/octet-stream';
                        payload.body = new Blob([fs.readFileSync(filePath)], { type: contentType });
                    }
                }
                const errorMessage = `File ${file} #${count}: ${object.id}`;
                tasks.push(() => this.performWithRetry(async () => {
                    const id = await this.createOrUpdateObject(object);
                    ids.push(id);
                }, errorMessage));
                // promises.push(createOrUpdateObject(client, object)); // COR-384
            }
        }
        await this.performTasks(tasks);
        return ids;
    }

    async createOrUpdateObjectPayload(payloadFile: string): Promise<void> {
        const filename = path.basename(payloadFile);
        const objectFile = this.payloadToObjectFileMap[filename];
        if (!objectFile) return;
        if (objectFile === 'design') {
            await this.updateDesign();
        } else {
            await this.createOrUpdateObjects([ objectFile ]);
        }
    }

    async createOrUpdateObject(obj: CordraObject): Promise<string> {
        const exists = obj.id && await this.client.getOrNull(obj.id);
        if (!exists) {
            obj = await this.client.create(obj);
            console.log(`Created object: ${obj.id}`);
        } else {
            obj = await this.client.update(obj);
            console.log(`Updated object: ${obj.id}`);
        }
        return obj.id!;
    }

    private fetchAndSet(file: string, setter: (contents: string) => void): void {
        const filePath = path.resolve(this.rootDir, file);
        if (fs.existsSync(filePath)) {
            const contents = fs.readFileSync(filePath, 'utf8');
            setter(contents);
        }
    }

    async updateDesign(): Promise<void> {
        let found = false;
        let designAddition: DesignBits = {};
        const payloads: Payload[] = [];
        this.fetchAndSet('./design/design.json', contents => {
            designAddition = JSON.parse(contents);
            found = true;
        });
        this.fetchAndSet('./design/design.js', contents => {
            designAddition.javascript = contents;
            found = true;
        });
        this.fetchAndSet('./design/CordraDesign.js', contents => {
            designAddition.builtInTypes = designAddition.builtInTypes || {};
            designAddition.builtInTypes.CordraDesign = designAddition.builtInTypes.CordraDesign || {};
            designAddition.builtInTypes.CordraDesign.javascript = contents;
            found = true;
        });
        this.fetchAndSet('./design/Schema.js', contents => {
            designAddition.builtInTypes = designAddition.builtInTypes || {};
            designAddition.builtInTypes.Schema = designAddition.builtInTypes.Schema || {};
            designAddition.builtInTypes.Schema.javascript = contents;
            found = true;
        });
        this.fetchAndSet('./design/handleMintingConfig.js', contents => {
            designAddition.handleMintingConfig = designAddition.handleMintingConfig || {};
            designAddition.handleMintingConfig.javascript = contents;
            found = true;
        });
        this.fetchAndSet('./design/payloads.json', contents => {
            const watchList = JSON.parse(contents);
            for (const payload of watchList) {
                if (!payload.filename) {
                    console.warn(`Skipping payload without filename: ${payload.name}`);
                    continue;
                }
                this.payloadToObjectFileMap[payload.filename] = 'design';
                const filePath = path.resolve(this.rootDir, './payloads', payload.filename);
                const contentType = payload.mediaType || 'application/octet-stream';
                payload.body = new Blob([fs.readFileSync(filePath)], { type: contentType });
                payloads.push(payload);
                found = true;
            }
        });
        if (!found) {
            console.log('No design found in ' + this.rootDir);
            return;
        }
        const design = await this.client.get('design');
        if (design.content.builtInTypes && designAddition.builtInTypes) {
            Object.assign(design.content.builtInTypes, designAddition.builtInTypes);
            delete designAddition.builtInTypes;
        }
        Object.assign(design.content, designAddition); // FIXME this needs to be a deep copy... or does it?
        if (this.client.baseUri.startsWith("http:")) {
            if (!design.content.allowInsecureAuthentication) {
                console.warn("Turning on allowInsecureAuthentication due to use of http: Cordra baseUri");
            }
            design.content.allowInsecureAuthentication = true;
        }
        if (payloads.length > 0) {
            design.payloads = payloads;
        }
        await this.client.update(design);
        console.log('Updated design object');
    }

    async loadDefaultTypesAndDesign(): Promise<void> {
        await this.client.update(defaultDesign);
        for (const type of defaultTypes.results) {
            await this.client.create(type);
        }
        console.log('Reset types');
    }
}
