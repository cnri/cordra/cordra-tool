export function catchAndReturnErrors(f: (...args: any[]) => unknown) {
    // Return instead of throwing so that errors can be displayed in Jest
    return function (...args: any[]): unknown {
        try {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            return f(...args);
        } catch (e: any) {
            console.error(e);
            e.testError = true;
            Object.defineProperty(e, 'message', {
                enumerable: true
            });
            return e;
        }
    };
}
